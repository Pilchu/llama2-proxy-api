# llama2-proxy-api

## Description
This is simple bottle app that initialize LLaMA2 localy and makes as a proxy http api for it. It supports returning whole response and streaming as well.
It was created only for fun and education reasons, so it has no authentication and security implemented. Do not use it at any producton environments. 

## Installation
Create virtualenv

    mkvirtualenv llama-api
    workon llama-api

and install requirements:

    pip install -r requrements.txt

## Usage
Run it with at least path to model data:

    python api.py /path/to/your/llama/llama-2-13b-chat.Q5_K_M.gguf

you can also add arguments:

    --chat-format - llama-2 by default
    --host - host you want to run (default: localhost)
    --port - port to listen on (default: 8080)
    --debug - run bottle in debug mode (default: False)

## License
https://www.gnu.org/licenses/gpl-3.0.html

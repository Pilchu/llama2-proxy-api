import argparse
import json

from bottle import request, route, run, response
from llama_cpp import Llama

llm = None


@route("/llama2/chat", ["post"])
def chat_llama2():
    if request.json.get("stream", False):
        try:
            response.content_type = "text/event-stream"
            response.flush = True
            chat_iter = llm.create_chat_completion(**request.json)
            for part in chat_iter:
                yield json.dumps(part) + "\n"
        except Exception as e:
            print(e)
    else:
        llm.create_chat_completion(**request.json)
        return llm.create_chat_completion(**request.json)


def init_model(model, chat_format):
    return Llama(model_path=model, chat_format=chat_format, n_ctx=0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Proxy API for LLaMA2")

    parser.add_argument("model", type=str, help="Path to model file like llama-2.gguf")
    parser.add_argument(
        "--chat-format", type=str, default="llama-2", help="Chat format"
    )
    parser.add_argument("--host", type=str, default="localhost", help="Server host")
    parser.add_argument("--port", type=int, default=8080, help="Port to listen on")
    parser.add_argument("--debug", type=str, default="False", help="Run in debug mode")

    args = parser.parse_args()

    llm = init_model(args.model, args.chat_format)

    run(
        host=args.host,
        port=args.port,
        debug=(args.debug.lower() in ("true", "yes", "on", "1")),
    )
